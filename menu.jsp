<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Прокат Велосипедов</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="_container-maxwidth">
    <div class="header_region">
        <div class="header_name">
            <h1>Прокат велосипедов</h1>
        </div>
        <div class="contact_information">
            <h3 class="phone">Наш телефон: +788005553535</h3>
            <h3 class="address">Наш адресс: г. Электросталь ул. Велосипедная</h3>
        </div>
    </div>
    <div class="around-border">
        <div class="bicycle-question">
            <div class="bicycle-type"><a href="#">Велосипеды в наличии</a></div>
            <div class="ask-question"><a href="#">Задать вопрос</a></div>
        </div>
        <div class="terms_region">
            <div class="terms_name">
                <h1>Условия и цены</h1>
            </div>
            <div class="prices_table">
                <table class="table_terms">
                    <tr>
                        <td>Количество часов</td>
                        <th>Стоимость</th>
                    </tr>
                    <tr>
                        <td>1 час</td>
                        <th>200 у.е.</th>
                    </tr>
                    <tr>
                        <td>5 часов</td>
                        <th>700 у.е.</th>
                    </tr>
                    <tr>
                        <td>12 часов</td>
                        <th>1000 у.е.</th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="btn-region">
            <button class="btn call-button" type="submit">Заказать обратный звонок</button>
        </div>
    </div>
</div>
</body>
</html>
